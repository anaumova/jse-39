package ru.tsc.anaumova.tm.exception.field;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Incorrect index...");
    }

}