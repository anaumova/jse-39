package ru.tsc.anaumova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.api.repository.IRepository;
import ru.tsc.anaumova.tm.model.AbstractModel;

import java.util.Collection;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

}